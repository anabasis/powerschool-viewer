module codeberg.org/anabasis/powerschool-viewer

go 1.17

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/gdamore/tcell/v2 v2.5.4
	github.com/rivo/tview v0.0.0-20230104153304-892d1a2eb0da
	github.com/tidwall/gjson v1.14.4
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/rivo/uniseg v0.4.3 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/term v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
)
