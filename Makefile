NAME=powerschool-viewer

all: run

build:
	go build -trimpath -ldflags="-w -s" .

run:
	go build -trimpath -ldflags="-w -s" -o ${NAME} .
	./${NAME}

install:
	go install -trimpath -ldflags="-w -s"

upgrade:
	go get -u ./...

clean:
	go clean
	rm ./builds/${NAME}*
	rm ${NAME}

lint:
	golangci-lint run --enable-all

vet:
	go vet

fmt:
	gofmt -s -w .
