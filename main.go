// Copyright 2022 anabasis
// SPDX-License-Identifier: GPL-3.0-or-later
/*
   This file is part of powerschool-viewer.

   powerschool-viewer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

   powerschool-viewer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with powerschool-viewer. If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	"github.com/tidwall/gjson"
)

var (
	baseURL    *url.URL
	homeURL    url.URL
	client     *http.Client
	configDir  *string
	configFile string
)

type info struct {
	Domain   string `json:"domain"`
	Username string `json:"username"`
}

const userAgent = "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0"

func main() {
	dir, err := os.UserConfigDir()
	if err != nil {
		log.Fatalln(err)
	}

	dir += filepath.FromSlash("/powerschool")
	configDir = flag.String("c", dir, "config directory")
	flag.Parse()

	configFile = *configDir + filepath.FromSlash("/config.json")

	jar, err := cookiejar.New(nil)
	if err != nil {
		log.Fatalln(err)
	}

	client = &http.Client{Jar: jar}

	tview.Styles.PrimitiveBackgroundColor = tcell.ColorDefault
	app = tview.NewApplication()

	loginScreen()

	if err := app.EnableMouse(true).Run(); err != nil {
		fmt.Printf("Error running application: %s\n", err)
	}
}

func login(domain, username, password string) (err error) {
	if len(domain) == 0 {
		return errors.New("invalid domain")
	}

	baseURL, err = url.Parse("https://" + domain)
	if err != nil {
		return err
	}

	if baseURL.Host != domain {
		return errors.New("parsed host does not match user-supplied domain")
	}

	homeURL = *baseURL
	homeURL.Path = "/guardian/home.html"

	if len(username) == 0 || len(password) == 0 {
		return errors.New("invalid username or password")
	}

	formData := url.Values{
		"dbpw":                     {password},
		"translator_username":      {""},
		"translator_password":      {""},
		"translator_ldapppassword": {""},
		"returnUrl":                {""},
		"serviceName":              {"PS Parent Portal"},
		"serviceTicket":            {""},
		"pcasServerUrl":            {"/"},
		"credentialType":           {"User Id and Password Credential"},
		"ldappassword":             {password},
		"request_locale":           {"en_US"},
		"account":                  {username},
		"pw":                       {password},
		"translatorpw":             {""},
	}

	req, err := http.NewRequest(http.MethodPost, homeURL.String(), strings.NewReader(formData.Encode()))
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("User-Agent", userAgent)

	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	for _, x := range client.Jar.Cookies(baseURL) {
		if x.Name == "psaid" {
			err = setLogin(domain, username)
			if err != nil {
				return err
			}

			return nil
		}
	}

	return errors.New("login was unsuccessful")
}

func mainData() (tableData, linkData [][]string, name string) {
	req, err := http.NewRequest(http.MethodGet, homeURL.String(), nil)
	if err != nil {
		log.Fatalln(err)
	}

	req.Header.Set("User-Agent", userAgent)

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	row, linkRow := []string{}, []string{}

	doc.Find(`table.linkDescList.grid tr.center.th2 th[rowspan="2"]`).Each(func(_ int, s *goquery.Selection) {
		row = append(row, s.Text())
		linkRow = append(linkRow, "")
	})
	row, linkRow = row[:len(row)-2], linkRow[:len(linkRow)-2]
	tableData, linkData = append(tableData, row), append(linkData, linkRow)

	doc.Find(".linkDescList.grid tr.center:not(.th2)").Each(func(_ int, s *goquery.Selection) {
		row, linkRow = []string{}, []string{}
		s.Find("td").Each(func(i int, x *goquery.Selection) {
			if i == 0 || i > 10 {
				var text, link string
				_, isInfo := x.Attr("align")

				switch {
				case i == 0:
					text = x.Text()
					link = ""
				case isInfo:
					text = strings.ReplaceAll(x.Text(), "  Email ", ": ")
					link = ""
				case x.Text() == " Not available":
					text = "N/A"
					link = ""
				default:
					html, err := x.Contents().Html()
					if err != nil {
						log.Fatalln(err)
					}

					text = regexp.MustCompile(`</?br/?>.*`).ReplaceAllString(html, "")

					link, _ = x.Find("a").Attr("href")
				}

				row = append(row, text)
				linkRow = append(linkRow, link)
			}
		})
		row, linkRow = row[:len(row)-2], linkRow[:len(linkRow)-2]
		tableData, linkData = append(tableData, row), append(linkData, linkRow)
	})

	name = doc.Find("li#userName span").Text()

	return
}

func classData(path string) (tableData [][]string, classInfo string) {
	req, err := http.NewRequest(http.MethodGet, baseURL.String()+"/guardian/"+path, nil)
	if err != nil {
		log.Println(err)
	}

	req.Header.Set("User-Agent", userAgent)

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
	}
	defer resp.Body.Close()

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	infoData := doc.Find("table.linkDescList tr.center td").Map(func(i int, s *goquery.Selection) string {
		if i == 4 {
			split := strings.Split(s.Text(), "\n")

			return regexp.MustCompile(`.$|\s*`).ReplaceAllString(split[1], "")
		}

		return s.Text()
	})

	classInfo = fmt.Sprintf("%s\n%s - %s%%", infoData[0], infoData[3], infoData[4])

	a, _ := doc.Find("div.xteContentWrapper").Attr("data-ng-init")
	a = regexp.MustCompile(`'|;$|\s*`).ReplaceAllString(a, "")
	b := make(map[string]string)

	for _, x := range strings.Split(a, ";") {
		y := strings.Split(x, "=")
		b[y[0]] = y[1]
	}

	if b["beginningDate"] == "" || b["endingDate"] == "" {
		tableData = [][]string{{"No data available"}}

		return
	}

	start, err := time.Parse("01/02/2006", b["beginningDate"])
	if err != nil {
		log.Fatalln(err)
	}
	end, err := time.Parse("01/02/2006", b["endingDate"])
	if err != nil {
		log.Fatalln(err)
	}

	sectionID, _ := doc.Find("div.xteContentWrapper > div").Attr("data-sectionid")

	data := fmt.Sprintf(
		`{"section_ids":[%s],"student_ids":[%s],"start_date":"%s","end_date":"%s"}`,
		sectionID,
		strings.Replace(b["studentFRN"], "001", "", 1),
		start.Format("2006-01-02"),
		end.Format("2006-01-02"),
	)

	lookupURL := *baseURL
	lookupURL.Path = "/ws/xte/assignment/lookup"

	req, err = http.NewRequest(http.MethodPost, lookupURL.String(), strings.NewReader(data))
	if err != nil {
		log.Println(err)
	}

	req.Header.Set("User-Agent", userAgent)
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Referer", baseURL.String())

	resp, err = client.Do(req)
	if err != nil {
		log.Println(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	jsonBody := gjson.GetBytes(body, "#._assignmentsections.0")
	tableData = append(
		tableData,
		[]string{
			"Due Date",
			"Category",
			"Assignment",
			"Collected",
			"Late",
			"Missing",
			"Exempt",
			"Absent",
			"Incomplete",
			"Score",
			"%",
			"Grade",
			"Info",
		},
	)

	for _, j := range jsonBody.Array() {
		date, err := time.Parse("2006-01-02", j.Get("duedate").String())
		if err != nil {
			log.Fatalln(err)
		}

		score := j.Get("_assignmentscores.0.scorepoints").String()
		if score == "" {
			score = "--"
		}

		var isCollected, isLate, isMissing, isExempt, isAbsent, isIncomplete string
		if j.Get("_assignmentscores.0.iscollected").Bool() {
			isCollected = "✔"
		}
		if j.Get("_assignmentscores.0.islate").Bool() {
			isLate = "✔"
		}
		if j.Get("_assignmentscores.0.ismissing").Bool() {
			isMissing = "✔"
		}
		if j.Get("_assignmentscores.0.isabsent").Bool() {
			isAbsent = "✔"
		}
		if j.Get("_assignmentscores.0.isincomplete").Bool() {
			isIncomplete = "✔"
		}
		if j.Get("_assignmentscores.0.iscollected").Bool() {
			isCollected = "✔"
		}

		tableData = append(tableData,
			[]string{
				date.Format("01/02/2006"),
				j.Get("_assignmentcategoryassociations.0._teachercategory.name").String(),
				j.Get("name").String(),
				isCollected,
				isLate,
				isMissing,
				isExempt,
				isAbsent,
				isIncomplete,
				score + "/" + j.Get("totalpointvalue").String(),
				fmt.Sprintf("%.2f", j.Get("_assignmentscores.0.scorepercent").Float()),
				j.Get("_assignmentscores.0.scorelettergrade").String(),
				j.Get("_assignmentscores.0._assignmentscorecomment.commentvalue").String(),
			},
		)
	}

	sort.Slice(tableData, func(i, j int) bool {
		return tableData[j][0] < tableData[i][0]
	})

	return
}

func setLogin(domain, username string) error {
	dirInfo, err := os.Stat(*configDir)
	if os.IsNotExist(err) {
		err = os.Mkdir(*configDir, 0o755)
		if err != nil {
			return err
		}
	} else if !dirInfo.IsDir() {
		return errors.New(*configDir + " is not a directory")
	}

	if _, err := os.Stat(configFile); os.IsNotExist(err) {
		if _, err = os.Create(configFile); err != nil {
			return err
		}
	}

	fileData := info{domain, username}

	out, err := json.Marshal(fileData)
	if err != nil {
		return err
	}

	err = os.WriteFile(configFile, out, 0o600)
	if err != nil {
		return err
	}

	return nil
}

func getLogin() (string, string) {
	fileData := info{}

	file, _ := os.ReadFile(configFile)
	json.Unmarshal(file, &fileData)

	return fileData.Domain, fileData.Username
}
