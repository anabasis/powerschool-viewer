// Copyright 2022 anabasis
// SPDX-License-Identifier: GPL-3.0-or-later
/*
   This file is part of powerschool-viewer.

   powerschool-viewer is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

   powerschool-viewer is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with powerschool-viewer. If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
)

var app *tview.Application

func loginScreen() {
	form := tview.NewForm()
	form.AddInputField("Domain", "", 20, nil, nil).
		AddInputField("Username", "", 20, nil, nil).
		AddPasswordField("Password", "", 10, '*', nil).
		AddButton("Submit", func() {
			err := login(
				form.GetFormItem(0).(*tview.InputField).GetText(),
				form.GetFormItem(1).(*tview.InputField).GetText(),
				form.GetFormItem(2).(*tview.InputField).GetText(),
			)
			if err != nil {
				modal := tview.NewModal().
					SetText(fmt.Sprint(err)).
					AddButtons([]string{"OK"}).
					SetDoneFunc(func(_ int, _ string) {
						form.GetFormItem(0).(*tview.InputField).SetText("")
						form.GetFormItem(1).(*tview.InputField).SetText("")
						form.GetFormItem(2).(*tview.InputField).SetText("")

						app.SetRoot(form, false).SetFocus(form)
					})
				app.SetRoot(modal, false).SetFocus(modal)
				return
			}
			mainScreen()
		}).
		AddButton("Quit", func() {
			app.Stop()
		})
	form.SetBorder(true).SetTitle("PowerSchool Login")
	app.SetRoot(form, true).SetFocus(form)

	domain, username := getLogin()
	form.GetFormItem(0).(*tview.InputField).SetText(domain)
	form.GetFormItem(1).(*tview.InputField).SetText(username)
}

func mainScreen() {
	tableData, linkData, name := mainData()

	table := tview.NewTable().SetBorders(true)
	cols, rows := len(tableData[0]), len(linkData)
	for r := 0; r < rows; r++ {
		for c := 0; c < cols; c++ {
			color := tcell.ColorDodgerBlue
			if c == 1 {
				color = tcell.ColorWhite
			}
			if c < 1 || r < 1 {
				color = tcell.ColorYellow
			}
			if tableData[r][c] == "N/A" {
				color = tcell.ColorGray
			}
			table.SetCell(r, c, tview.NewTableCell(tableData[r][c]).SetTextColor(color).SetAlign(tview.AlignCenter))
		}
	}
	table.
		SetFixed(1, 1).
		SetSelectable(true, true).
		Select(1, 2).
		SetSelectedFunc(func(row int, column int) {
			if linkData[row][column] != "" {
				table.GetCell(row, column).SetTextColor(tcell.ColorCornflowerBlue)
				table.SetSelectable(false, false)
				classScreen(linkData[row][column])
			}
		}).
		SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
			if event.Rune() == 'q' {
				app.Stop()
				return nil
			}
			return event
		})

	frame := tview.NewFrame(table)
	frame.SetBorder(true).SetTitle(fmt.Sprintf("PowerSchool Home - %s", name))

	app.SetRoot(frame, true).SetFocus(table)
}

func classScreen(path string) {
	data, classInfo := classData(path)

	table := tview.NewTable().SetBorders(true)

	button := tview.NewButton("Back")
	button.
		SetSelectedFunc(func() {
			mainScreen()
		}).
		SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
			if event.Key() == tcell.KeyTab {
				app.SetFocus(table)
				return nil
			} else if event.Rune() == 'q' {
				mainScreen()
				return nil
			}
			return event
		})

	text := tview.NewTextView().
		SetTextAlign(tview.AlignCenter).
		SetText(classInfo)

	cols, rows := len(data[0]), len(data)
	for r := 0; r < rows; r++ {
		for c := 0; c < cols; c++ {
			color := tcell.ColorWhite
			if r < 1 {
				color = tcell.ColorYellow
			}

			align := tview.AlignCenter
			if c <= 2 {
				align = tview.AlignLeft
			}

			table.SetCell(r, c, tview.NewTableCell(data[r][c]).SetTextColor(color).SetAlign(align))
		}
	}

	table.SetFixed(1, 0).SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyTab {
			app.SetFocus(button)
			return nil
		} else if event.Rune() == 'q' {
			mainScreen()
			return nil
		}
		return event
	})

	grid := tview.NewGrid().
		SetRows(3, 0).
		SetColumns(10, 0).
		AddItem(button, 0, 0, 1, 1, 0, 0, false).
		AddItem(text, 0, 1, 1, 2, 0, 0, false).
		AddItem(table, 1, 0, 1, 3, 0, 0, true)

	app.SetRoot(grid, true).SetFocus(table)
}
