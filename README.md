# powerschool-viewer

A TUI client for PowerSchool to view grades and assignments.

Install by running `go install codeberg.org/anabasis/powerschool-viewer@latest`.
Run `powerschool-viewer` and sign in to view the account details.

You will need to provide the domain of the PowerSchool server (like `powerschool.example.com`), username, and password to sign in.

## Acknowledgments
* [tview](https://github.com/rivo/tview) (&copy; 2018 by Oliver Kuederle under the [MIT License](https://github.com/rivo/tview/blob/master/LICENSE.txt))
* [goquery](https://github.com/PuerkitoBio/goquery) (&copy; 2012-2021 by Martin Angers & Contributors under the [BSD 3-Clause License](https://github.com/PuerkitoBio/goquery/blob/master/LICENSE))
* [gjson](https://github.com/tidwall/gjson) (&copy; 2016 by Josh Baker under the [MIT License](https://github.com/tidwall/gjson/blob/master/LICENSE))
* [tcell](https://github.com/gdamore/tcell) (&copy; by The TCell Authors under the [Apache License, Version 2.0](https://github.com/gdamore/tcell/blob/master/LICENSE))
* [Go language](https://go.dev) (&copy; 2009 by The Go Authors under the [BSD 3-Clause License](https://go.dev/LICENSE))

## License
powerschool-viewer is licensed under the GNU Public License, version 3 or any later version; see `LICENSE` for more details.
